import React from 'react';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {Tabs} from './src/navigation/Tabs';

const GESTURE_HANDLER_ROOT_VIEW_STYLES = {flex: 1};

const App = () => {
  return (
    <GestureHandlerRootView style={GESTURE_HANDLER_ROOT_VIEW_STYLES}>
      <NavigationContainer>
        <Tabs />
      </NavigationContainer>
    </GestureHandlerRootView>
  );
};

export default App;
