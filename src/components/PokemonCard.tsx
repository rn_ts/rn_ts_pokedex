import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Image,
  Pressable,
} from 'react-native';
import ImageColors from 'react-native-image-colors';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';

import {Pokemon} from '../interfaces/pokemonInterfaces';
import {FadeInImage} from '../components/FadeInImage';

import {RootStackParams} from '../navigation/HomeTab';

const {width} = Dimensions.get('window');

interface Props {
  pokemon: Pokemon;
}

export const PokemonCard = ({pokemon}: Props) => {
  const [bgColor, setBackgroundColor] = useState<string>('gray');
  const isMounted = useRef(true);
  const navigation = useNavigation<StackNavigationProp<RootStackParams>>();

  useEffect(() => {
    const getColors = async () => {
      const result = await ImageColors.getColors(pokemon.picture, {
        fallback: 'gray',
      });
      if (!isMounted.current) {
        return;
      }
      const color =
        result.platform === 'ios' ? result.background : result.dominant;
      setBackgroundColor(color || 'gray');
    };
    getColors();
    return () => {
      isMounted.current = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onPressPokemon = () =>
    navigation.navigate('PokemonScreen', {pokemon, color: bgColor});

  return (
    <Pressable onPress={onPressPokemon}>
      <View
        style={[
          styles.cardContainer,
          {width: width * 0.4, backgroundColor: bgColor},
        ]}>
        <View>
          <Text style={styles.name}>
            {pokemon.name}
            {`\n#${pokemon.id}`}
          </Text>
        </View>

        <View style={styles.pokeballContainer}>
          <Image
            source={require('../assets/pokebola-blanca.png')}
            style={styles.pokeball}
          />
        </View>

        <FadeInImage uri={pokemon.picture} style={styles.picture} />
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    marginHorizontal: 10,
    marginBottom: 25,
    height: 120,
    width: 20,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  name: {
    fontSize: 20,
    color: 'white',
    top: 10,
    left: 20,
    fontWeight: 'bold',
  },
  pokeballContainer: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    opacity: 0.5,
    overflow: 'hidden',
  },
  pokeball: {
    bottom: -20,
    right: -20,
    height: 100,
    width: 100,
  },
  picture: {
    width: 120,
    height: 120,
    position: 'absolute',
    right: -9,
    top: -30,
  },
});
