import React from 'react';
import {ScrollView, Text, View, StyleSheet} from 'react-native';
import {PokemonDetail} from '../interfaces/pokemonInterfaces';
import {FadeInImage} from './FadeInImage';

interface Props {
  pokemon: PokemonDetail;
}

export const PokemonDetails = ({pokemon}: Props) => {
  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={[StyleSheet.absoluteFillObject]}
      contentContainerStyle={styles.mainContainer}>
      <View style={styles.firstContainer}>
        <Text style={styles.title}>Types</Text>
        <Text style={styles.regularText}>
          <View style={styles.row}>
            {pokemon.types.map(({type}) => (
              <Text key={type.name} style={styles.regularText}>
                {`${type.name} `}
              </Text>
            ))}
          </View>
        </Text>
      </View>

      <View style={styles.container}>
        <Text style={styles.title}>Weight</Text>
        <Text style={styles.regularText}>{pokemon.weight} Kg</Text>
      </View>

      <View style={styles.container}>
        <Text style={styles.title}>Sprites</Text>
      </View>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        <FadeInImage
          uri={pokemon.sprites.front_default}
          style={styles.basicSprite}
        />
        <FadeInImage
          uri={pokemon.sprites.back_default}
          style={styles.basicSprite}
        />
        <FadeInImage
          uri={pokemon.sprites.front_shiny}
          style={styles.basicSprite}
        />
        <FadeInImage
          uri={pokemon.sprites.back_shiny}
          style={styles.basicSprite}
        />
      </ScrollView>

      <View style={styles.container}>
        <Text style={styles.title}>Base Skills</Text>
        <Text style={styles.regularText}>
          <View style={styles.row}>
            {pokemon.abilities.map(({ability}) => (
              <Text key={ability.name} style={styles.regularText}>
                {`${ability.name} `}
              </Text>
            ))}
          </View>
        </Text>
      </View>

      <View style={styles.container}>
        <Text style={styles.title}>Moves</Text>
        <Text style={[styles.regularText]}>
          {pokemon.moves.map(({move}) => (
            <Text key={move.name} style={styles.regularText}>
              {`${move.name} `}
            </Text>
          ))}
        </Text>
      </View>

      <View style={styles.container}>
        <Text style={styles.title}>Stats</Text>
        {pokemon.stats.map((stat, i) => (
          <View key={`${stat.stat.name}-${i}`} style={styles.row}>
            <Text
              style={[
                styles.regularText,
                styles.column,
              ]}>{`${stat.stat.name} `}</Text>
            <Text
              style={[
                styles.regularText,
                styles.column,
                styles.boldText,
              ]}>{`${stat.base_stat} `}</Text>
          </View>
        ))}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    paddingBottom: 50,
  },
  firstContainer: {
    marginTop: 390,
    paddingHorizontal: 15,
  },
  container: {
    paddingHorizontal: 15,
    flex: 1,
  },
  title: {
    marginTop: 30,
    fontSize: 30,
    fontWeight: 'bold',
  },
  row: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap',
    flexShrink: 1,
  },
  column: {
    flex: 1,
  },
  regularText: {
    fontSize: 19,
    marginRight: 10,
  },
  boldText: {
    fontWeight: 'bold',
  },
  basicSprite: {
    width: 100,
    height: 100,
  },
});
