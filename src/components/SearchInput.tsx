import React, {useEffect, useState} from 'react';
import {
  Platform,
  StyleProp,
  StyleSheet,
  TextInput,
  ViewStyle,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useDebouncedValue} from '../hooks/useDebouncedValue';

interface Props {
  onDebonce?: (value: string) => void;
  style?: StyleProp<ViewStyle>;
}

export const SearchInput = ({onDebonce, style}: Props) => {
  const [textValue, setTextValue] = useState<string>('');
  const debouncedValue = useDebouncedValue(textValue);

  useEffect(() => {
    onDebonce?.(debouncedValue);
  }, [debouncedValue, onDebonce]);

  return (
    <View style={[styles.container, style]}>
      <View style={styles.textBackground}>
        <TextInput
          placeholder="Search pokemon"
          onChangeText={setTextValue}
          style={styles.textInput}
          autoCapitalize="none"
          autoCorrect={false}
          value={textValue}
        />
        <Icon name="search-outline" size={30} color="grey" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // backgroundColor: 'red',
  },
  textBackground: {
    backgroundColor: '#f3f1f3',
    borderRadius: 50,
    height: 40,
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  textInput: {
    flex: 1,
    fontSize: 18,
    top: Platform.OS === 'ios' ? 0 : 2,
  },
});
