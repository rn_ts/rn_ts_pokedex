import {useState, useEffect} from 'react';
import {PokemonDetail} from '../interfaces/pokemonInterfaces';
import {pokemonAPI} from '../api/pokemonAPI';

const initialState = {
  isLoading: true,
  pokemon: {} as PokemonDetail,
};

interface StateType {
  isLoading: boolean;
  pokemon: PokemonDetail;
}

export const usePokemon = (id: string) => {
  const [state, setState] = useState<StateType>(initialState);
  const {isLoading, pokemon} = state;

  useEffect(() => {
    const getPokemonDetail = async () => {
      const response = await pokemonAPI.get<PokemonDetail>(
        `https://pokeapi.co/api/v2/pokemon/${id}`,
      );
      setState({isLoading: false, pokemon: response.data});
    };
    getPokemonDetail();
  }, [id]);

  return {
    isLoading,
    pokemon,
  };
};
