import {useEffect, useRef, useState} from 'react';
import {pokemonAPI} from '../api/pokemonAPI';
import {
  Pokemon,
  PokemonResponse,
  Result,
} from '../interfaces/pokemonInterfaces';

export const usePokemonList = () => {
  const [pokemonList, setPokemonList] = useState<Pokemon[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const nextPageUrl = useRef('https://pokeapi.co/api/v2/pokemon?limit=40');

  const mapPokemonList = (list: Result[]): Pokemon[] =>
    list.map(({name, url}) => {
      const urlParts = url.split('/').filter(Boolean);
      const id = urlParts[urlParts.length - 1];
      const picture = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`;
      return {id, name, picture};
    });

  const getPokemons = async () => {
    setIsLoading(true);
    const {data} = await pokemonAPI.get<PokemonResponse>(nextPageUrl.current);
    nextPageUrl.current = data.next;
    const newPokemons = mapPokemonList(data.results);
    setPokemonList([...pokemonList, ...newPokemons]);
    setIsLoading(false);
  };

  useEffect(() => {
    getPokemons();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    pokemonList,
    getPokemons,
    isLoading,
  };
};
