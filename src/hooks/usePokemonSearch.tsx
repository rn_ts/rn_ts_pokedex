import {useEffect, useState} from 'react';
import {pokemonAPI} from '../api/pokemonAPI';
import {
  Pokemon,
  PokemonResponse,
  Result,
} from '../interfaces/pokemonInterfaces';

export const usePokemonSearch = () => {
  const [pokemonList, setPokemonList] = useState<Pokemon[]>([]);
  const [isFetching, setIsFetching] = useState<boolean>(true);

  const mapPokemonList = (list: Result[]): Pokemon[] =>
    list.map(({name, url}) => {
      const urlParts = url.split('/').filter(Boolean);
      const id = urlParts[urlParts.length - 1];
      const picture = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`;
      return {id, name, picture};
    });

  const getPokemons = async () => {
    const {data} = await pokemonAPI.get<PokemonResponse>(
      'https://pokeapi.co/api/v2/pokemon?limit=1200',
    );
    const newPokemons = mapPokemonList(data.results);
    setPokemonList(newPokemons);
    setIsFetching(false);
  };

  useEffect(() => {
    getPokemons();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    pokemonList,
    isFetching,
  };
};
