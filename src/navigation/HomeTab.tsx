import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {HomeScreen} from '../screens/HomeScreen';
import {PokemonScreen} from '../screens/PokemonScreen';
import {Pokemon} from '../interfaces/pokemonInterfaces';

export type HomeRootStackParams = {
  HomeScreen: undefined;
  PokemonScreen: {
    pokemon: Pokemon;
    color: string;
  };
};

const Stack = createStackNavigator<HomeRootStackParams>();

export const HomeTab = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyle: {
          backgroundColor: 'white',
        },
      }}>
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="PokemonScreen" component={PokemonScreen} />
    </Stack.Navigator>
  );
};
