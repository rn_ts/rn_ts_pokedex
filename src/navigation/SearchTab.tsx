import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Pokemon} from '../interfaces/pokemonInterfaces';
import {SearchScreen} from '../screens/SearchScreen';
import {PokemonScreen} from '../screens/PokemonScreen';

export type SearchRootStackParams = {
  SearchScreen: undefined;
  PokemonScreen: {
    pokemon: Pokemon;
    color: string;
  };
};

const Stack = createStackNavigator<SearchRootStackParams>();

export const SearchTab = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyle: {
          backgroundColor: 'white',
        },
      }}>
      <Stack.Screen name="SearchScreen" component={SearchScreen} />
      <Stack.Screen name="PokemonScreen" component={PokemonScreen} />
    </Stack.Navigator>
  );
};
