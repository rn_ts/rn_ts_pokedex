import React from 'react';
import {Platform, StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import {SearchTab} from './SearchTab';
import {HomeTab} from './HomeTab';

const Tab = createBottomTabNavigator();

export const Tabs = () => {
  return (
    <Tab.Navigator
      sceneContainerStyle={styles.sceneContainerStyle}
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: '#5853D6',
        tabBarLabelStyle: styles.labelStyle,
        tabBarStyle: styles.tabBarStyle,
      }}>
      <Tab.Screen
        name="HomeTab"
        component={HomeTab}
        options={{
          tabBarLabel: 'List',
          tabBarIcon: ({color}) => (
            <Icon color={color} size={25} name="list-outline" />
          ),
        }}
      />
      <Tab.Screen
        name="SearchTab"
        component={SearchTab}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: ({color}) => (
            <Icon color={color} size={25} name="search-outline" />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  sceneContainerStyle: {
    backgroundColor: 'white',
  },
  labelStyle: {
    marginBottom: Platform.OS === 'android' ? 10 : 0,
  },
  tabBarStyle: {
    borderWidth: 0,
    elevation: 0,
    height: Platform.OS === 'android' ? 60 : 80,
    position: 'absolute',
    backgroundColor: 'rgba(255, 255, 255, 0.90)',
  },
});
