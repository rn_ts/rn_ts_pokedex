import React, {useCallback} from 'react';
import {Image, ActivityIndicator, Text, View} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {styles} from '../theme/appTheme';
import {usePokemonList} from '../hooks/usePokemonList';
import {Pokemon} from '../interfaces/pokemonInterfaces';
import {PokemonCard} from '../components/PokemonCard';

export const HomeScreen = () => {
  const {top} = useSafeAreaInsets();
  //
  const {pokemonList, getPokemons} = usePokemonList();

  const keyExtractor = useCallback(({id}: Pokemon) => `${id}`, []);

  const renderItem = useCallback(
    ({item}: {item: Pokemon}) => <PokemonCard pokemon={item} />,
    [],
  );

  return (
    <>
      <Image
        source={require('../assets/pokebola.png')}
        style={styles.pokebolaBG}
      />
      <View style={styles.listContainer}>
        <FlatList
          data={pokemonList}
          keyExtractor={keyExtractor}
          renderItem={renderItem}
          onEndReached={getPokemons}
          onEndReachedThreshold={0.4}
          showsVerticalScrollIndicator={false}
          numColumns={2}
          ListFooterComponent={
            <ActivityIndicator
              size={20}
              color={'gray'}
              style={styles.loadingHeight}
            />
          }
          ListHeaderComponent={
            <Text
              style={[
                styles.title,
                styles.globalMargin,
                {top: top + 20, marginBottom: top + 30},
              ]}>
              Pokedex
            </Text>
          }
        />
      </View>
    </>
  );
};
