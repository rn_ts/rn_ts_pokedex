import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ActivityIndicator,
} from 'react-native';
import {RootStackParams} from '../navigation/HomeTab';
import {StackScreenProps} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Ionicons';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {FadeInImage} from '../components/FadeInImage';
import {usePokemon} from '../hooks/usePokemon';
import {PokemonDetails} from '../components/PokemonDetails';

interface Props extends StackScreenProps<RootStackParams, 'PokemonScreen'> {}

export const PokemonScreen = ({navigation, route}: Props) => {
  const {pokemon, color} = route.params;
  const {name, id, picture} = pokemon;
  const {top} = useSafeAreaInsets();
  const {isLoading, pokemon: pokemonDetail} = usePokemon(id);
  return (
    <View style={styles.container}>
      <View style={[styles.headerContainer, {backgroundColor: color}]}>
        {/* Back button */}
        <TouchableOpacity
          style={[styles.backButton, {top: top + 10}]}
          onPress={() => navigation.goBack()}>
          <Icon size={30} name="arrow-back-outline" color="white" />
        </TouchableOpacity>

        {/* Pokemon name */}
        <Text style={[styles.pokemonName, {top: top + 40}]} numberOfLines={2}>
          {name}
          {`\n#${id}`}
        </Text>

        <Image
          source={require('../assets/pokebola-blanca.png')}
          style={styles.pokeball}
        />

        <FadeInImage uri={picture} style={styles.pokemonImage} />
      </View>

      {/* Details */}
      {isLoading ? (
        <View style={styles.loadingContainer}>
          {<ActivityIndicator color={color} size={'large'} />}
        </View>
      ) : (
        <PokemonDetails pokemon={pokemonDetail} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    height: 370,
    zIndex: 999,
    alignItems: 'center',
    borderBottomRightRadius: 1000,
    borderBottomLeftRadius: 1000,
  },
  backButton: {
    position: 'absolute',
    left: 30,
  },
  pokemonName: {
    fontSize: 40,
    color: 'white',
    alignSelf: 'flex-start',
    left: 20,
  },
  pokeball: {
    width: 250,
    height: 250,
    bottom: -20,
    opacity: 0.5,
  },
  pokemonImage: {
    width: 250,
    height: 250,
    position: 'absolute',
    bottom: -50,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
