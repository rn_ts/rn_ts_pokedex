import React, {useCallback, useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Platform,
  Text,
  FlatList,
  Dimensions,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {SearchInput} from '../components/SearchInput';
import {usePokemonSearch} from '../hooks/usePokemonSearch';
import {PokemonCard} from '../components/PokemonCard';
import {Pokemon} from '../interfaces/pokemonInterfaces';
import {styles as GlobalStyles} from '../theme/appTheme';
import {Loading} from '../components/Loading';

const {width} = Dimensions.get('window');

export const SearchScreen = () => {
  const {top} = useSafeAreaInsets();
  const {pokemonList, isFetching} = usePokemonSearch();
  const [term, setTerm] = useState<string>('');
  const [filteredPokemon, setFilteredPokemon] = useState<Pokemon[]>([]);

  const keyExtractor = useCallback(({id}: Pokemon) => `${id}`, []);

  const renderItem = useCallback(
    ({item}: {item: Pokemon}) => <PokemonCard pokemon={item} />,
    [],
  );

  useEffect(() => {
    if (term.length === 0) {
      return setFilteredPokemon([]);
    }

    if (isNaN(Number(term))) {
      return setFilteredPokemon(
        pokemonList.filter(p =>
          p.name.toLocaleLowerCase().includes(term.toLowerCase()),
        ),
      );
    }

    const pokemonByID = pokemonList.find(p => p.id === term);
    setFilteredPokemon(pokemonByID ? [pokemonByID] : []);
  }, [pokemonList, term]);

  if (isFetching) {
    return <Loading />;
  }

  return (
    <View style={[styles.container]}>
      <SearchInput
        onDebonce={setTerm}
        style={[
          styles.searchInputStyle,
          {top: Platform.OS === 'ios' ? top : top + 10},
        ]}
      />
      <FlatList
        data={filteredPokemon}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
        showsVerticalScrollIndicator={false}
        numColumns={2}
        style={styles.listStyle}
        contentContainerStyle={styles.listContentStyle}
        ListHeaderComponent={
          <Text
            style={[
              GlobalStyles.title,
              {marginTop: Platform.OS === 'ios' ? top + 30 : top + 40},
            ]}>
            {term}
          </Text>
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
  },
  listStyle: {
    paddingTop: 20,
  },
  listContentStyle: {
    paddingBottom: 150,
  },
  searchInputStyle: {
    position: 'absolute',
    width: width - 40,
    zIndex: 999,
  },
});
